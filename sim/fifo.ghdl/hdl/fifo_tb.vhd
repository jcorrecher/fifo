-- * header
-------------------------------------------------------------------------------
-- Title      : Test Bench FIFO
-- Project    :
-------------------------------------------------------------------------------
-- File       : fifo_tb.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-07-23
-- Last update: 2021-06-01
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-07-23  1.0      jcorrecher  Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * entity declaration
-------------------------------------------------------------------------------

entity fifo_tb is

  generic (
    G_CLK1_PERIOD : natural := 10;
    G_CLK2_PERIOD : natural := 10;
    G_DEPTH_L2    : natural := 10;
    G_TSIM        : real    := 100000.0);

end entity fifo_tb;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture sim of fifo_tb is

-- ** constant declaration

-- *** data size
  constant C_BYTE_W : natural := 8;
-- *** clock period definition
  constant C_CLK1_P : time    := G_CLK1_PERIOD * 1 ns;
  constant C_CLK2_P : time    := G_CLK2_PERIOD * 1 ns;

-- ** signal declaration
  signal tbc_clk1_s     : std_logic                     := '0';
  signal tbc_clk2_s     : std_logic                     := '0';
  signal tbc_rst_s      : std_logic                     := '1';
  signal tbc_wstart_s   : std_logic                     := '0';
  signal tbc_rstart_s   : std_logic                     := '0';
  signal tbc_endsim_s   : std_logic                     := '0';
  --
  signal tb0_full_s     : std_logic                     := '0';
  signal tb0_rdy_s      : std_logic                     := '0';
  signal tb0_empty_s    : std_logic                     := '0';
  signal tb0_data_s     : unsigned(C_BYTE_W-1 downto 0) := (others => '0');
  --
  signal tb1_ena_r      : std_logic                     := '0';
  signal tb1_data_r     : unsigned(C_BYTE_W-1 downto 0) := (others => '0');
  --
  signal tb2_ena_r      : std_logic                     := '0';
  signal tb2_ena_s      : std_logic                     := '0';
  signal tb2_data_r     : unsigned(C_BYTE_W-1 downto 0) := (others => '0');
  --
  signal tbl_cnt_err_r  : unsigned(31 downto 0)         := (others => '0');
  signal tbl_cnt_data_r : unsigned(31 downto 0)         := (others => '0');

begin

-------------------------------------------------------------------------------
-- * TBC : control signals
-------------------------------------------------------------------------------
--! This block creates control signals as clocks and reset

  --! write domain clock generation
  tbc_clk1_s   <= not tbc_clk1_s after C_CLK1_P/2;
  --! read domain clock generation
  tbc_clk2_s   <= not tbc_clk2_s after C_CLK2_P/2;
  --! reset generation (asynchronously)
  tbc_rst_s    <= '0'            after 1 us;
  --! write domain start time
  tbc_wstart_s <= '1'            after 150 * C_CLK1_P;
  --! read domain start time
  tbc_rstart_s <= '1'            after 170 * C_CLK2_P;
  --! assert when simulation time is finished
  tbc_endsim_s <= '1'            after G_TSIM * 1 us;

-------------------------------------------------------------------------------
-- * TB0 : design unit under test
-------------------------------------------------------------------------------
--! Fifo instance will be declared in this block

  u_duut : entity work.fifo
    generic map (
      G_DEPTH_L2 => G_DEPTH_L2)
    port map (
      rst      => tbc_rst_s,
      wclk     => tbc_clk1_s,
      ic_wena  => tb1_ena_r,
      oc_full  => tb0_full_s,
      id_write => tb1_data_r,
      rclk     => tbc_clk2_s,
      ic_rena  => tb2_ena_s,
      oc_valid => tb0_rdy_s,
      oc_empty => tb0_empty_s,
      od_read  => tb0_data_s);

-------------------------------------------------------------------------------
-- * TB1 : write data
-------------------------------------------------------------------------------
--! This block generates enable and data for write clock domain. The
--! accumulator value will be stored in the FIFO during write enable is asserted
--! when FIFO is full, write enable get frozen.

  process (tbc_clk1_s) is
  begin
    if rising_edge(tbc_clk1_s) then
      if (tbc_rst_s = '1') then
        tb1_ena_r  <= '0';
        tb1_data_r <= (others => '0');
      else
        --! the write data will be a counter
        if (tb1_ena_r = '1') then
          tb1_data_r <= tb1_data_r + 1;
        end if;
        --! write enable asserted when fifo is not full
        if (tbc_wstart_s = '1' and tb0_full_s = '0') then
          tb1_ena_r <= '1';
        else
          tb1_ena_r <= '0';
        end if;
      end if;
    end if;
  end process;

-------------------------------------------------------------------------------
-- * TB2 : read data
-------------------------------------------------------------------------------
--! This block generates enable and expected read data for read clock domain.
--! The accumulator value will be compared with read data from FIFO during read
--! enable is asserted. when FIFO is empty, read enable get frozen.

  process (tbc_clk2_s) is
  begin
    if rising_edge(tbc_clk2_s) then
      if (tbc_rst_s = '1') then
        tb2_ena_r  <= '0';
        tb2_data_r <= (others => '0');
      else
        --! expected data generated with read enable
        if (tb0_rdy_s = '1') then
          tb2_data_r <= tb2_data_r + 1;
        end if;
        --! start read when assert read start
        if (tbc_rstart_s = '1') then
          tb2_ena_r <= '1';
        end if;
      end if;
    end if;
  end process;

--! read enable asserted when fifo is not empty
  tb2_ena_s <= tb2_ena_r and not tb0_empty_s;

-------------------------------------------------------------------------------
-- * TBL : generate logs
-------------------------------------------------------------------------------
--! test logs are located here


  --! generate log when read/write domain starts
  assert tbc_wstart_s = '1' report "write domain starts" severity note;
  assert tbc_rstart_s = '1' report "read domain starts" severity note;

  --! generate log when full or empty are asserted
  assert tb0_full_s = '1' report "FIFO is full" severity warning;
  assert tb0_empty_s = '1' report "FIFO is empty" severity warning;

  process (tbc_clk2_s) is
  begin
    if rising_edge(tbc_clk2_s) then
      --! expected data differs from read data when read data is ready
      if rising_edge(tb0_rdy_s) then
        assert tb0_data_s /= tb2_data_r report "read data " & to_hstring(tb0_data_s) & "differs to expected data " & to_hstring(tb2_data_r) severity warning;
      end if;
    end if;
  end process;

  process (tbc_clk2_s) is
  begin
    if rising_edge(tbc_clk2_s) then
      if (tbc_rst_s = '1') then
        tbl_cnt_err_r  <= (others => '0');
        tbl_cnt_data_r <= (others => '0');
      else
        if (tb0_rdy_s = '1') then
          tbl_cnt_data_r <= tbl_cnt_data_r + 1;
          if (tb0_data_s /= tb2_data_r) then
            tbl_cnt_err_r <= tbl_cnt_err_r + 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  scoreboard: process
  begin

    --! wait until simulation finishes
    wait until rising_edge(tbc_endsim_s);
    --! show scoreboard
    report "==SIMULATION SCOREBOARD==" severity note;
    report "Total data read: " & to_hstring(tbl_cnt_data_r) severity note;
    report "Total errors counter: " & to_hstring(tbl_cnt_err_r) severity note;
    wait;
  end process scoreboard;

end architecture sim;
