-- * header
-------------------------------------------------------------------------------
-- Title      : FIFO
-- Project    :
-------------------------------------------------------------------------------
-- File       : fifo.vhd
-- Author     : Jose Correcher  <jcorrecher@gmail.com>
-- Company    :
-- Created    : 2020-05-25
-- Last update: 2020-09-17
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module contains a FIFO with empty and full control signals.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-25  1.0      jcorrecher Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.function_pkg.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity fifo is

  generic (
    G_DEPTH_L2 : natural := 10);

  port (
    rst      : in  std_logic;
    wclk     : in  std_logic;
    ic_wena  : in  std_logic;
    oc_full  : out std_logic;
    id_write : in  unsigned;
    rclk     : in  std_logic;
    ic_rena  : in  std_logic;
    oc_valid : out std_logic;
    oc_empty : out std_logic;
    od_read  : out unsigned);

end entity fifo;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of fifo is

-- ** constant declaration
  constant C_UNUSEDB : unsigned(id_write'range) := (others => '0');

-- ** type declaration
  type T_ADDR is array (0 to 1) of unsigned(G_DEPTH_L2 downto 0);

-- ** signal declaration
  signal b0_cnt_r     : unsigned(G_DEPTH_L2 downto 0) := (others => '0');
  signal b0_raddr_ra  : T_ADDR                        := (others => (others => '0'));
  signal b0_waddr_r   : unsigned(G_DEPTH_L2 downto 0) := (others => '0');
  signal b0_raddr_s   : unsigned(G_DEPTH_L2 downto 0) := (others => '0');
  signal b0_wena_s    : std_logic                     := '0';
  signal b0_full_s    : std_logic                     := '0';
  --
  signal b1_cnt_r     : unsigned(G_DEPTH_L2 downto 0) := (others => '0');
  signal b1_waddr_ra  : T_ADDR                        := (others => (others => '0'));
  signal b1_raddr_r   : unsigned(G_DEPTH_L2 downto 0) := (others => '0');
  signal b1_waddr_s   : unsigned(G_DEPTH_L2 downto 0) := (others => '0');
  signal b1_rena_s    : std_logic                     := '0';
  signal b1_valid_r   : std_logic                     := '0';
  signal b1_empty_s   : std_logic                     := '0';
  --
  signal b2_unuseda_s : unsigned(od_read'range)       := (others => '0');

begin

-------------------------------------------------------------------------------
-- * B0 : write domain
-------------------------------------------------------------------------------
--! This block handles write domain control signals. write address is
--! increasing when enable is asserted and the memory is not full. In addition,
--! full control is obtained from read and write address comparison.
--! Read and write address contains 1bit extra, used in full/empty logic comparison.
--! Gray and binary conversion will be used in order to avoid metastability
--! when counters crossing to another clock domain.

  process (wclk) is
  begin
    if rising_edge(wclk) then
      if (rst = '1') then
        b0_cnt_r <= (others => '0');
      else
        --! increase write address when enable is asserted
        if (b0_wena_s = '1') then
          b0_cnt_r <= b0_cnt_r + 1;
        end if;
      end if;
      --! clock domain crossing for read address
      b0_raddr_ra <= b1_raddr_r & b0_raddr_ra(0);
      --! binary to gray conversion for write address
      b0_waddr_r  <= f_b2g(b0_cnt_r);
    end if;
  end process;

  --! write enable depends on input enable and full memory
  b0_wena_s <= ic_wena and not b0_full_s;

  --! gray to binary conversion
  b0_raddr_s <= f_g2b(b0_raddr_ra(1));

  --! full logic : signal control asserts when read/write address are the same, but extra
  --! bit are different
  b0_full_s <= '1' when (b0_raddr_s(G_DEPTH_L2-1 downto 0) = b0_cnt_r(G_DEPTH_L2-1 downto 0) and
                         b0_raddr_s(b0_raddr_s'high) /= b0_cnt_r(b0_cnt_r'high)) else
               '0';

-------------------------------------------------------------------------------
-- * B1 : read domain
-------------------------------------------------------------------------------
--! This block handles read domain control signals. read address is increasing
--! when enable is asserted and is not empty. in addition, empty control is
--! obtained from read an write address comparison.
--! Read and write address contains 1bit extra, used in full/empty logic comparison.
--! Ggray and binary conversion will be used in order to avoid metastability
--! when counters crossing to another clock domain.

  process (rclk) is
  begin
    if rising_edge(rclk) then
      if (rst = '1') then
        b1_cnt_r <= (others => '0');
      else
        --! increase read address when enable is asserted
        if (b1_rena_s = '1') then
          b1_cnt_r <= b1_cnt_r + 1;
        end if;
      end if;
      --! data valid is read enable delayed
      b1_valid_r  <= b1_rena_s;
      --! clock domain crossing for write address
      b1_waddr_ra <= b0_waddr_r & b1_waddr_ra(0);
      --! binary to gray conversion for write address
      b1_raddr_r  <= f_b2g(b1_cnt_r);
    end if;
  end process;

  --! read enable depends on input enable and empty memory
  b1_rena_s <= ic_rena and not b1_empty_s;

  --! gray to binary conversion
  b1_waddr_s <= f_g2b(b1_waddr_ra(1));

  --! empty logic : signal control asserts when read/write address and extra
  --! bit are the same
  b1_empty_s <= '1' when (b1_waddr_s = b1_cnt_r) else '0';

-------------------------------------------------------------------------------
-- * B2 : dual-port ram instance
-------------------------------------------------------------------------------
--! Dual port template has been instanced in that module. port a will be used
--! for write domain. port b will be used for read domain.

  u_dp_ram : entity work.dual_port_ram
    port map (
      clka     => wclk,
      clkb     => rclk,
      ic_wena  => b0_wena_s,
      ic_wenb  => '0',
      ic_rena  => '0',
      ic_renb  => b1_rena_s,
      ic_addra => b0_cnt_r(G_DEPTH_L2-1 downto 0),
      ic_addrb => b1_cnt_r(G_DEPTH_L2-1 downto 0),
      id_porta => id_write,
      id_portb => C_UNUSEDB,
      od_porta => b2_unuseda_s,
      od_portb => od_read);

-------------------------------------------------------------------------------
-- * assign internal signals to port
-------------------------------------------------------------------------------

  oc_full  <= b0_full_s;
  oc_empty <= b1_empty_s;
  oc_valid <= b1_valid_r;

end architecture rtl;
