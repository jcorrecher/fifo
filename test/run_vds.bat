@echo off

setlocal enableDelayedExpansion

REM
REM set default values
REM
:setDefValues
set vivado_path=C:\Xilinx\Vivado\2018.3
set design_name=fifo_test
set fpga_device=xc7s25csga324-1
set num_cores=4

REM
REM show menu when no arguments are setted
REM
:noInputArgs
if "%~1"=="" (
    goto helpMenu
)


REM
REM loop searching input arguments
REM
:searchArgs

set arg=%1

REM options starts with a hyphen

set argh=!arg:~0,1!

if [!argh!] == [-] (
	
	REM run tcl script
	if /i [!arg!] == [-run] (
		goto run
	) else if /i [!arg!] == [-clean] (
		goto clean
	) else (
		echo Unexpected argument !arg!
		exit /b
	)
	
	goto searchArgs

)
	

REM 
REM show menu
REM

:helpMenu

echo.
echo Run vivado synthesis and implementation for test
echo.
echo Description:
echo This batch script runs a tcl script to synthesize and implement test design and obtain reports.
echo.
echo Syntax:
echo "run_vds.bat [-run] [-clean]"
echo.
echo Usage:
echo Name    Description
echo --------------------------------------
echo -run    Launch tcl script to create a test project, sinthesize, implement and generate reports.
echo -clean  Clean project folder        

goto eof

REM 
REM run project
REM

:run

%vivado_path%\bin\vivado -nolog -nojournal -notrace -mode tcl -source ./lib/vds_test.tcl

goto eof

REM 
REM clean project
REM

:clean

@RD /s /q "prj"
echo project folder deleted


:eof