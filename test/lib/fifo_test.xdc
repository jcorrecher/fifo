# Xilinx Design Constraints

# create clock period

create_clock -period 10.000 -name ts_wclk [get_ports wclk]
create_clock -period 10.000 -name ts_rclk [get_ports rclk]

# asynchronous clock relationship

set_clock_groups -name async_wclk_rclk -asynchronous -group ts_wclk -group ts_rclk
