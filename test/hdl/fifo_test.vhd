-- * header
-------------------------------------------------------------------------------
-- Title      : FIFO test
-- Project    :
-------------------------------------------------------------------------------
-- File       : top.vhd
-- Author     : Jose Correcher  <jocorso@jocor-virtualbox>
-- Company    :
-- Created    : 2020-07-28
-- Last update: 2020-07-28
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This file wraps FIFO with registered signals to validate that
--              the design is synthesizable
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-07-28  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity fifo_test is

  generic (
    G_DEPTH_L2 : natural := 10;
    G_DATA_W   : natural := 18);

  port (
    rst      : in  std_logic;
    wclk     : in  std_logic;
    ic_wena  : in  std_logic;
    oc_full  : out std_logic;
    id_write : in  unsigned(G_DATA_W-1 downto 0);
    rclk     : in  std_logic;
    ic_rena  : in  std_logic;
    oc_valid : out std_logic;
    oc_empty : out std_logic;
    od_read  : out unsigned(G_DATA_W-1 downto 0));

end entity fifo_test;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture test of fifo_test is

-- ** signal declaration
  signal b0_ena_r   : std_logic                := '0';
  signal b0_data_r  : unsigned(id_write'range) := (others => '0');
  --
  signal b1_full_s  : std_logic                := '0';
  signal b1_valid_s : std_logic                := '0';
  signal b1_empty_s : std_logic                := '0';
  signal b1_data_s  : unsigned(od_read'range)  := (others => '0');
  --
  signal b2_ena_r   : std_logic                := '0';

begin

-------------------------------------------------------------------------------
-- * B0 : write clock domain
-------------------------------------------------------------------------------

  process (wclk) is
  begin
    if rising_edge(wclk) then
      b0_ena_r  <= ic_wena;
      b0_data_r <= id_write;
      oc_full   <= b1_full_s;
    end if;
  end process;

-------------------------------------------------------------------------------
-- * B1 : component instance
-------------------------------------------------------------------------------

  i_fifo : entity work.fifo
    generic map (
      G_DEPTH_L2 => G_DEPTH_L2)
    port map (
      rst      => rst,
      wclk     => wclk,
      ic_wena  => b0_ena_r,
      oc_full  => b1_full_s,
      id_write => b0_data_r,
      rclk     => rclk,
      ic_rena  => b2_ena_r,
      oc_valid => b1_valid_s,
      oc_empty => b1_empty_s,
      od_read  => b1_data_s);

-------------------------------------------------------------------------------
-- * B2 : output register
-------------------------------------------------------------------------------

  process (rclk) is
  begin
    if rising_edge(rclk) then
      b2_ena_r <= ic_rena;
      oc_valid <= b1_valid_s;
      oc_empty <= b1_empty_s;
      od_read  <= b1_data_s;
    end if;
  end process;

end architecture test;
